#!/bin/bash
set -o errexit
set -o   xtrace

TARGET_DIR=$PWD/target
mkdir -p $TARGET_DIR

cd gitfolder


cp server.js $TARGET_DIR
cp package.json $TARGET_DIR
cp .npmrc $TARGET_DIR
cp manifest-dev.yml $TARGET_DIR/

cp .env.dev $TARGET_DIR/

ls -lart




