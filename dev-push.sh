#!/bin/bash
set -o errexit
set -o xtrace

pwd
cd ../../..
pwd
ls


mv .env.dev .env
ls -lart

APP_ENV=CI
npm config set registry https://artifactory.platform.manulife.io/artifactory/api/npm/npm
npm install
npm run build
ls -lart

#cp -R build $TARGET_DIR/build
cp -R build ../target
pwd

cd ../target
pwd
ls -lart


#npm rebuild

cf push -f manifest-dev.yml -c "node server.js"

cd build
ls 
 

